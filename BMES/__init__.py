"""
The flask application package.
"""
import os
from flask import request,session, Flask
from BMES.SharedBp import db
from flask_migrate import Migrate
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView 
from BMES.CatalogueBp.models import Brand, Category, Product 
from BMES.CatalogueBp.views import catalogue

basedir = os.path.abspath(os.path.dirname(__file__))


app = Flask(__name__)
app.register_blueprint(catalogue)


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'bmeswebapp.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#registering the product catalogue models to admin modules
admin = Admin(app)
admin.add_view(ModelView(Product, db.session))
admin.add_view(ModelView(Category, db.session))
admin.add_view(ModelView(Brand, db.session))

db.init_app(app)
migrate = Migrate(app, db)

import BMES.views

